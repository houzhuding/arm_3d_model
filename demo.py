#!/usr/bin/env python

"""
arm 3d model based on each pose

@author: houzhu.ding
date  : 12/10/2021

Note:

1. need put link 0-7 obj files in visual/link0-7.obj
2. the output is triangles surfaces of each link, every 3 points defines a triangle surface
3. parameters are robot poses, for eample, q = [0,-40,0,90,80,130,0], is home position
"""
import numpy as np 
from matplotlib import pyplot as plt 
import open3d as o3d
from arm_3d_model_pose_dep import arm_3d_model_faces

# define a posture (based on the MDH, if GEN, A6_GEN = A6_MDH-90)
q = [0,40,0,90,0,130,0] # home pose 
# q = [0,0,0,0,0,90,0] # vertical pose
# q = [0,90,90,90,90,90,0] # orthogonal pose

# get the arm 3d model surface under this posture
arm_3d_model = arm_3d_model_faces(q)
face_each_link = arm_3d_model.face_each_link # faces of all 8 links

# concatenate all points in 3 rows and visualize the robot in open3d
x = []
y = []
z = []
for i in range(8):
	x.append(face_each_link['link'+str(i)]['x'])
	y.append(face_each_link['link'+str(i)]['y'])
	z.append(face_each_link['link'+str(i)]['z'])

x_all = np.concatenate(x, axis=0)
y_all = np.concatenate(y, axis=0)
z_all = np.concatenate(z, axis=0)
points_all = np.transpose([x_all,y_all,z_all])

pcd = o3d.geometry.PointCloud()
pcd.points = o3d.utility.Vector3dVector(points_all)
# voxel_grid = o3d.geometry.VoxelGrid.create_from_point_cloud(pcd,voxel_size=0.001)
# o3d.visualization.draw_geometries([voxel_grid])
o3d.visualization.draw_geometries([pcd])

# plot each link in matplotlib
# fig = plt.figure()
# ax  = fig.add_subplot(projection = '3d')
# for i in range(8):
# 	x = face_each_link['link'+str(i)]['x']
# 	y = face_each_link['link'+str(i)]['y']
# 	z = face_each_link['link'+str(i)]['z']
# 	ax.plot(x,y,z,color='black')
# ax.set_box_aspect([1,1,1])
# plt.show()

