#!/usr/bin/env python

#############################################################
################### arm 3d model at  pose   #################
###################       12/06/2021        #################  
################### Author: Houzhu Ding     #################
#############################################################

"""
arm 3d model based on each pose

"""

import numpy as np 
from matplotlib import pyplot as plt 
from obj_file_reader import obj_reader
from obj_file_transform import obj_transform
from arm_kine_dyna import arm_kinematics

class arm_3d_model_faces(object):
	def __init__(self,q_input):
		# self.face_all_links = []
		self.face_each_link = {}
		# variables
		link_idx = 0
		face_per_link = []
		file_names = []
		
		# input q is in degrees
		if len(q_input)<7:
			print("Pose joint value error, robot will be at default pose")
			q = np.radians([0,-40,0,90,0,130,0]) # pose of the robot
		else:
			q = np.radians(q_input)

		# load files
		for i in range(0,8):
			file_names.append('visual\\link'+str(i)+'.obj')	
		# forward kinematics
		arm_kine = arm_kinematics()
		fk_gen_T = arm_kine.fkine_gen(q) # fk_gen_T is 4x4x8 for every GEN frame and flange frame

		# transform each link based on the input robot pose
		#start = time.time()
		for file_name in file_names:
			face_xyz = {}
			face_per_link = []
			
			link = obj_reader(file_name)
			print('Processing link',link_idx)
			# transformation 
			if link_idx > 0:
				T_gen_cur = fk_gen_T[:,:,link_idx-1]
			else:
				T_gen_cur = np.identity(4)
			
			link_trans = obj_transform(T_gen_cur,link.vertex_array)
			trans_v = link_trans.trans_v
			trans_f  = link.f_array
			# get all faces and points
			for f_idx in trans_f:
				face_per_link.append(trans_v[f_idx,0:3])

			face_per_link_arr = np.array(face_per_link)
			f_shape = face_per_link_arr.shape
			face_per_link_arr_reshape = face_per_link_arr.reshape(f_shape[0]*f_shape[1],f_shape[2])
			f_x = face_per_link_arr_reshape[:,0]
			f_y = face_per_link_arr_reshape[:,1]
			f_z = face_per_link_arr_reshape[:,2]
			face_xyz['x'] = f_x
			face_xyz['y'] = f_y
			face_xyz['z'] = f_z

			# self.face_all_links.append(face_per_link) 
			self.face_each_link['link'+str(link_idx)] = face_xyz
			link_idx += 1
		# end = time.time()
		# print('time=:',end - start)
	

