
### Arm 3D model Generation ###

* Get the 3D model of A02L arm at different poses
* v 1.0
* contact: houzhu.ding@flexiv.com

### Setup and run ###

* input: rotot position in MDH frame, e.g. q = [0,-40,0,90,80,130,0]
* pip install open3d
* pip install numpy
* run demo.py will visualize the arm 3d points

### Results ###
* Home pose
![Home pose](results/visualize_arm_in_open_3d_home_pose.PNG)
* Vertical pose
![Vertical pose](results/visualize_arm_in_open_3d_vertical_pose.PNG)
* Orthogonal pose
![Orthogonal pose](results/visualize_arm_in_open_3d_orthogonal_pose.PNG)