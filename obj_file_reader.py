#!/usr/bin/env python
"""
OBJ file loader
extract vertex, vertex normal, and face info from obj file

"""
import re 
import numpy as np 

class obj_reader(object):
	def __init__(self,obj_file_name):
		self.vertex_array = []
		self.vn_array     = []
		self.f_array      = []
		self.line_count = 0
		try:
			obj_contents = open(obj_file_name,'r')
			lines    = obj_contents.readlines()
			v_count  = 0
			vn_count = 0
			f_count  = 0
			f_array = []
			for line in lines:
				self.line_count += 1
				line_str = line.strip()
				# parse the vertex
				if line_str[0:2] == 'v ':
					v_count+=1
					vertex_str = re.findall('-?\d*\.?\d+(?:[Ee]\ *-?\ *[0-9]+)?',line_str)
					#"-?\ *[0-9]+\.?[0-9]*(?:[Ee]\ *-?\ *[0-9]+)?" this should also work,https://stackoverflow.com/questions/18152597/extract-scientific-number-from-string
					# print(vertex_str)
					vertex_float = [float(x) for x in vertex_str]
					vertex_float.append(1);
					vertex_float_vector = np.array(vertex_float)
					if v_count == 1:
						self.vertex_array = vertex_float_vector
					else:
						self.vertex_array = np.vstack((self.vertex_array,vertex_float_vector))
					# print('Vertex : ',vertex_float_vector)
				# parse the vertex normal
				if line_str[0:2] == 'vn':
					vn_count+=1
					vn_str = re.findall('-?\d*\.?\d+(?:[Ee]\ *-?\ *[0-9]+)?',line_str)
					# print(vn_str)
					vn_float = [float(x) for x in vn_str]
					vn_float.append(1);
					vn_float_vector = np.array(vn_float)
					if vn_count == 1:
						self.vn_array = vn_float_vector
					else:
						self.vn_array = np.vstack((self.vn_array,vn_float_vector))
					# print('Vertex normal: ',vn_float_vector)
				# parse the face index
				if line_str[0:2] == 'f ':
					f_count+=1
					f_str = re.findall('-?\d*\.?\d+(?:[Ee]\ *-?\ *[0-9]+)?',line_str)
					# print(f_str)
					f_vector = [int(x)-1 for x in f_str]
					# remove duplicates
					f_vector_unique = []
					for x in f_vector:
						if x not in f_vector_unique:
							f_vector_unique.append(x)
					if len(f_vector_unique) == 3:
						# f_vector_unique.extend([-1,-1])
						self.f_array.append(f_vector_unique)
					# print('Face: ',f_vector_unique)
			if v_count == vn_count:
				self.line_count = v_count+vn_count+f_count
			else:
				self.line_count = -1
		except IOError:
			print("File not found !")