#!/usr/bin/env python

#############################################################
################### save whole arm obj file #################
###################       12/09/2021        #################  
################### Author: Houzhu Ding     #################
#############################################################

"""
Save whole arm obj file from each individual obj file at 
certain pose defined by q


"""
# robot_file_name = 'robot_model_'+str(q)+'.obj';
with open(robot_file_name,'w') as obj_trans_contents:
	for file_name in file_names:
		link = obj_reader(file_name)

		# print(link.vertex_array)
		# print(link.vn_array)
		# print(link.f_array)
		print(link.line_count)

		# transformation 
		
		# link_T = gen_transform(link_idx,q)
		# [trans_v,trans_vn,tran_f] = obj_transform(link_T,link.vertex_array,link.vn_array,link.f_array,link.vertex_count)
		link_idx += 1
		trans_v  = link.vertex_array
		trans_vn = link.vn_array
		trans_f  = []
		for f_vector in link.f_array:
			f_vector = list(map(lambda x:x+vertex_total_count, f_vector))
			trans_f.append(f_vector)
		vertex_total_count = vertex_total_count + link.line_count

		# copy each link v,vn,f to a whole arm obj model
		# write the vertex
		for v in trans_v:
			obj_trans_line = 'v '+' '.join(str(round(i,5)) for i in v)+'\n'
			# write the line
			obj_trans_contents.write(obj_trans_line)
		# write the vertex normal, vn
		for vn in trans_vn:
			obj_trans_line = 'vn '+' '.join(str(round(i,5)) for i in vn)+'\n'
			# write the line
			obj_trans_contents.write(obj_trans_line)
		for f in trans_f:
			obj_trans_line = 'f '+str(f[0])+'//'+str(f[0])+' '+str(f[1])+'//'+str(f[1])+' '+str(f[2])+'//'+str(f[2])+'\n'
			# obj_trans_line = 'f ' +'/'.join(str(round(i,5)) for i in f)+'\n'
			# write the line
			obj_trans_contents.write(obj_trans_line)

		print(file_name,'-Done')