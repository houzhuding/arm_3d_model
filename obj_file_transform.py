#!/usr/bin/env python
"""
OBJ file loader
extract vertex, vertex normal, and face info from obj file

"""
import re 
import numpy as np 

class obj_transform(object):
	def __init__(self,T,v):
		self.trans_v = v

		# try:

		self.trans_v = np.transpose(np.dot(T,np.transpose(v)))
		# except:
		# 	print('Fail to transform')