#!/usr/bin/env python

#############################################################
################### arm kinematic dynamics  #################
###################       12/09/2021        #################  
################### author: Houzhu Ding     #################
#############################################################

"""
forward kinematics based on arm GEN frame

"""

"""
Updates: 

"""
import numpy as np 
from numpy.linalg import inv
import math 
# np.set_printoptions(precision=3)
class arm_kinematics(object):
	def __init__(self):
		self.fkine_gen_T = np.ones((4,4,8))

		
	def fkine_gen(self,theta_GEN):
		# MDH parameters
		pi = math.pi
		aph_nom = [0, pi/2, -pi/2, -pi/2, pi/2, pi/2, pi/2]
		a_nom 	= [0, 0, 0, 0.02, -0.02, 0, 0.11]
		d_nom 	= [0.365, 0.065, 0.395, 0.055, 0.385, 0.100, 0]
		# Transformation between MDH and GEN
		T_MDH2GEN = np.ones((4,4,8))
		T_MDH2GEN[:,:,0] = [[-1, 0, 0, 0],
		         			[0,-1, 0, 0],
		         			[0, 0, 1,-0.21],
		         			[0, 0, 0, 1]]
		T_MDH2GEN[:,:,1] = [[-1, 0, 0, 0],
		          			[0, 0, 1, 0],
		         			[0, 1, 0,-0.035],
		         		 	[0, 0, 0, 1]]
		T_MDH2GEN[:,:,2] = [ [-1, 0, 0, 0],
		         		 	[0,-1, 0, 0],
		         		 	[0, 0, 1,-0.19],
		         		 	[0, 0, 0, 1]]
		T_MDH2GEN[:,:,3] = [ [1, 0, 0, 0],
		         		 	[0, 0,-1, 0],
		         		 	[0, 1, 0,-0.025],
		         		 	[0, 0, 0, 1]]   
		T_MDH2GEN[:,:,4] = [ [-1, 0, 0, 0],
		         		 	[0,-1, 0, 0],
		         		 	[0, 0, 1,-0.19],
		         		 	[0, 0, 0, 1]]
		T_MDH2GEN[:,:,5] = [ [0, 0, 1, 0],
		         		 	[1, 0, 0, 0],
		         		 	[0, 1, 0,-0.07],
		         		 	[0, 0, 0, 1]]  
		T_MDH2GEN[:,:,6] = [ [1, 0, 0, 0],
		         		 	[0, 1, 0, 0],
		        		 	[0, 0, 1, 0.055],
		        		 	[0, 0, 0, 1]]
		T_MDH2GEN[:,:,7] = [[-1, 0, 0, 0],
		        		 	[0,-1, 0, 0],
		        			[0, 0, 1, 0.081],
		        			[0, 0, 0, 1]]
		# Transformation between MDH
		S_theta = np.ones(7)
		C_theta = np.ones(7)
		theta = theta_GEN
		T_basic = np.ones((4,4,8))
		for i in range(7):
		    ## MDH
		    C_theta[i] = math.cos(theta[i])
		    S_theta[i] = math.sin(theta[i])
		    
		    T_basic[:,:,i] = [[C_theta[i],                  -S_theta[i],                     0,                     			   a_nom[i]],
		                  [S_theta[i]*math.cos(aph_nom[i]),   C_theta[i]*math.cos(aph_nom[i]),    -math.sin(aph_nom[i]),      -math.sin(aph_nom[i])*d_nom[i]],
		                  [S_theta[i]*math.sin(aph_nom[i]),   C_theta[i]*math.sin(aph_nom[i]),     math.cos(aph_nom[i]),       math.cos(aph_nom[i])*d_nom[i]],
		                  [0,                            0,                              0,                     1                       ]]
		    # print(T_basic[:,:,i])
		T_basic[:,:,7] = [[1,0,0,0],[0,1,0,0],[0,0,1,0.136],[0,0,0,1]]
		# Transformation between MDH and GEN
		T_basic_GEN = np.zeros((4,4,8)) 
		T_basic_GEN[:,:,0] = np.dot(T_basic[:,:,0],T_MDH2GEN[:,:,0])
		for i in range(1,7):
			T_basic_GEN[:,:,i] = np.dot(np.dot(inv(T_MDH2GEN[:,:,i-1]),T_basic[:,:,i]),T_MDH2GEN[:,:,i])
		T_basic_GEN[:,:,7] = T_MDH2GEN[:,:,7]

		T_i2base_GEN = np.ones((4,4,8))
		fk_GEN = np.identity(4)
		for i in range(8):
			fk_GEN =  np.dot(fk_GEN,T_basic_GEN[:,:,i])
			T_i2base_GEN[:,:,i] = fk_GEN 

		# T_i2base_MDH = np.ones((4,4,8))
		# fk_MDH = np.identity(4)
		# for i in range(8):
		# 	fk_MDH =  np.dot(fk_MDH,T_basic[:,:,i])
		# 	T_i2base_MDH[:,:,i] = fk_MDH 
		# ee_pos_MDH = T_i2base_MDH[:,:,7].dot([0,0,0,1])
		# ee_pos_GEN = T_i2base_GEN[:,:,7].dot([0,0,0,1])
		# print(ee_pos_MDH,ee_pos_GEN)
		
		self.fkine_gen_T = T_i2base_GEN

		return self.fkine_gen_T